package com.example.genderapp

data class GenderModel(
    val count: Int? = null,
    val gender: String? = null,
    val name: String? = null,
    val probability: Float? = null
)
