package com.example.genderapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeFragment : Fragment() {

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val historyButton =  view.findViewById<Button>(R.id.history)
        historyButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_dataFragment)
        }

        val getGenderButton =  view.findViewById<Button>(R.id.get_gender)
        getGenderButton.setOnClickListener {
            val nameEdit = view.findViewById<EditText>(R.id.name)
            val name = nameEdit.text.toString()
            val genderText = view.findViewById<TextView>(R.id.gender)
            val notFoundText = "gender not found"
            if (name == "") {
                genderText.text = notFoundText
            }
            else {
                val serviceGenerator = ServiceGenerator.buildService(ApiService::class.java)
                val call = serviceGenerator.getGender(name)
                call.enqueue(object : Callback<GenderModel> {
                    override fun onResponse(
                        call: Call<GenderModel>,
                        response: Response<GenderModel>
                    ) {
                        if (response.isSuccessful){
                            val gender = response.body()?.gender
                            if (gender != null) {
                                val genderString = gender.toString()
                                genderText.text = genderString
                                val preferences: SharedPreferences = activity!!.getSharedPreferences("genderAppData", Context.MODE_PRIVATE)
                                val editor = preferences.edit()
                                editor.putString(name, genderString)
                                editor.apply()
                            }
                            else {
                                genderText.text = notFoundText
                            }
                        }
                        else {
                            genderText.text = notFoundText
                        }
                    }

                    override fun onFailure(call: Call<GenderModel>, t: Throwable) {
                        genderText.text = notFoundText
                    }
                })
            }
        }

        return view
    }
}