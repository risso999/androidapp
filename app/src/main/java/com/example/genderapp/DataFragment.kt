package com.example.genderapp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import androidx.navigation.fragment.findNavController
import kotlin.reflect.typeOf

class DataFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_data, container, false)

        val preferences: SharedPreferences = requireActivity().getSharedPreferences("genderAppData", Context.MODE_PRIVATE)
        var entries: List<String> = preferences.all.toList().map { "Name ${it.first} is ${it.second}" }
        for (item in entries) {
            println(item)
        }
        var myListView = view.findViewById<ListView>(R.id.list)
        val arrayAdapter: ArrayAdapter<String> =
            ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, entries)
        myListView.adapter = arrayAdapter

        val homeButton = view.findViewById<Button>(R.id.home)
        homeButton.setOnClickListener {
            findNavController().navigate(R.id.action_dataFragment_to_homeFragment)
        }

        val clearButton = view.findViewById<Button>(R.id.clear)
        clearButton.setOnClickListener {
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            arrayAdapter.clear()
        }

        return view
    }
}