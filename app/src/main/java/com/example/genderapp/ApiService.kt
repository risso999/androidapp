package com.example.genderapp

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/")
    fun getGender(@Query("name") name: String): Call<GenderModel>
}